# <div align="center"> SED and AWK </div>


**SED and AWK** are two text processing Functionality Provided by UNIX OS which mostly works on line by line text checking. UNIX Provides many such utilities for editing text and manipulating content without any help from a separate text editor. Basically, these commands help us to find any word or to delete them, etc. SED and AWK Mainly use regular expressions to find patterns and given words.

## SED
SED is a UNIX Command which stands for "***Stream Editor***".  SED is a streamlined Editor command which lets you edit any file using a different type of commands. We can perform lots of operations like searching, find and replace, Deletion and insertion, and different commands in SED which lets us edit the file without any different text editor. It works well with character-based processing. It takes a file as an input stream all in once which makes it more efficient. It also filters the text while processing. We can use SED at the command-line, or within a shell script.

Below are some examples of commands in SED.

1. **$ cat file.txt**

    ![](SED/a.png "")

    This command will Print all the data in the file.

  

2. **$ sed  's/unix/linux/' file.txt**

    ![](SED/b.png "")

    This command will Replace word “unix” with the word “linux”, but it will only replace the first occurrence in the line.
    If you want to replace second occurence use "**sed -e 's/unix/linux/2' my_file**" or for all occurrence use "**sed -e 's/unix/linux/g' my_file**".

  

3. **$ sed  '1,3 s/unix/linux/' file.txt**

    ![](SED/c.png "")

	This command will Replace word “unix” with the word “linux” in between line 1 & 3.

  

4. **$ sed ‘/learn/d’ file.txt**

    ![](SED/d.png "")

	This command will delete all the lines which contains "learn".

  
  


## AWK

 AWK is one of the powerful and useful commands for processing and analyzing the files which are mostly organized by rows and columns for e.g.: - Tables. It is named after its creators i.e. Aho, Weinberger & Kernighan. An awk program mainly works on each line of an input file. AWK has a BEGIN{} section of where commands are  used before processing any content of the file, the main {} section which works on each line of the file and there is an END{} section that happens after the file processing has been finished.
 
For Example:-

 1. **$ awk '{print}' student.txt**

    ![](AWK/2.png "")

	 Above command will print all the content in the given file because there is no pattern is given.
	 
2. **$ awk '/arts/ {print}' student.txt** 

    ![](AWK/3.png "")

	 Above command will print only the line matches to the given pattern"arts".

3. **$ awk '{print $1,$3}' student.txt**

    ![](AWK/5.png "")

	 In the above example we have printed only first and third column data.
	 
4. **$ awk 'NR`==`2, NR`==`5 {print NR,$0}' student.txt**

    ![](AWK/4.png "")

	Above command will print specified rows between given range.


    ## References

    - https://www-users.york.ac.uk/~mijp1/teaching/2nd_year_Comp_Lab/guides/grep_awk_sed.pdf

    - https://www.geeksforgeeks.org/awk-command-unixlinux-examples/

    - https://www.youtube.com/watch?v=ixOiOS35HYg&t=268s